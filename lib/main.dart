import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MyOwnLayout',
      theme: ThemeData(
        primarySwatch: Colors.black12,
      ),
      home: const MyHomePage(title: '1'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final myController_user = TextEditingController();
  final myController_pwd = TextEditingController();
  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController_user.dispose();
    myController_pwd.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        color: Colors.black54,
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
          child: Column(
            children: [
            CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/Cat.jpg),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    'Row1',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  Text(
                    'Row2',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(30),
                child: Text(
                  "ลงชื่อเข้าใช้",
                  style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15,right: 15,bottom: 30),
                child: TextField(
                  controller: myController_user,
                  decoration: InputDecoration(
                    hintText: 'ชื่อผู้ใช้',
                    fillColor: Colors.white,
                    filled: true,
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15,right: 15,bottom: 30),
                child: TextField(
                  controller: myController_pwd,
                  decoration: InputDecoration(
                    hintText: 'รหัสผ่าน'
                    fillColor: Colors.white,
                    filled: true,
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.green,
                        onPrimary: Colors.white,
                        padding:EdgeInsets.symmetric(horizontal:7,vertical: 7),
                        textStyle: TextStyle(fontSize: 8),
                      ),
                      onPressed:(){
                        myController_user.clear();
                        myController_pwd.clear();
                      },
                      child:Text('Cancle'),
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.black,
                        onPrimary: Colors.white,
                        padding:EdgeInsets.symmetric(horizontal:7,vertical: 7),
                        textStyle: TextStyle(fontSize: 15),
                      ),
                      onPressed:()=> displayToast(),
                      //print('Hello Login!!!!'),
                      child:Text('Confirm'),
                    ),
                  ]
              ),

            ],
          ),
        ),
      ),
    );
  }//end build

  void displayToast(){
    String username =myController_user.text;
    String password =myController_pwd.text;

    Fluttertoast.showToast(
      msg:'User:$username \n Password:$password',
      toastLength: Toast.LENGTH_SHORT,

    );
  }
}//end class
